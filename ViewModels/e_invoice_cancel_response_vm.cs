﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EwayBill.LogitaxAPImodule.EInvoicing
{
    public class e_invoice_cancel_response_vm
    {
        public string Irn { get; set; }
        public string CancelDate { get; set; }
    }
}
