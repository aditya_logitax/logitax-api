﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EwayBill.LogitaxAPImodule.EInvoicing
{
    public class e_invoice_value_detail_vm
    {
        public decimal? AssVal { get; set; }
        public decimal? CgstVal { get; set; }
        public decimal? SgstVal { get; set; }
        public decimal? IgstVal { get; set; }
        public decimal? CesVal { get; set; }
        public decimal? StCesVal { get; set; }
        public decimal? RndOffAmt { get; set; }
        public decimal? TotInvVal { get; set; }
        public decimal? TotInvValFc { get; set; }
        public decimal? Discount { get; set; }
        public decimal? OthChrg { get; set; }
    }
}
