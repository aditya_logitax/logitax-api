﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EwayBill.LogitaxAPImodule.EInvoicing
{
    public class get_IRN_details_by_docdetails_vm
    {
        public string CLIENTCODE { get; set; }
        public string USERCODE { get; set; }
        public string PASSWORD { get; set; }
        public string RequestorGSTIN { get; set; }
        public List<docdetails_vm> docdetailslist { get; set; }
    }

    public class docdetails_vm
    {
        public string doctype { get; set; }
        public string docnum { get; set; }
        public string docdate { get; set; }
    }
}
