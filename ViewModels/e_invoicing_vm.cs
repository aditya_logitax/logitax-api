﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EwayBill.LogitaxAPImodule.EInvoicing
{
    public class e_invoicing_vm
    {
        public e_invoicing_detail_vm data { get; set; }
    }

    public class e_invoice_generate_irn_request_vm
    {
        public string Data { get; set; }
    }
    public class e_invoicing_detail_vm
    {
        public string UserName { get; set; }
        public string Password { get; set; }
        public string AppKey { get; set; }
        public bool ForceRefreshAccessToken { get; set; }
    }

    public class e_invoicing_response_vm
    {
        public int Status { get; set; }
        public List<e_invoicing_detail_response_vm> ErrorDetails { get; set; }
        public e_invoicing_response_data Data { get; set; } 
    }

    public class e_invoicing_detail_response_vm
    {
        public string ErrorCode { get; set; }
        public string ErrorMessage { get; set; }
        public string AckNo { get; set; }
        public string AckDt { get; set; }
        public string Irn { get; set; }
        public ewb_data_response_1 Dcy_Data { get; set; }
        public List<e_invoicing_detail_response_vm> ErrorDetails { get; set; }
        public List<e_invoice_info_details> InfoDtls { get; set; }
    }

    public class e_invoicing_response_data
    {
        public string ClientId { get; set; }
        public string UserName { get; set; }
        public string AuthToken { get; set; }
        public string Sek { get; set; }
        public DateTime TokenExpiry { get; set; }
        public string DecryptSek { get; set; }
    }
}
