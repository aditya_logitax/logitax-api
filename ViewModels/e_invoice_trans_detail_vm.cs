﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EwayBill.LogitaxAPImodule.EInvoicing
{
    public class e_invoice_trans_detail_vm
    {
        public string TaxSch { get; set; }
        public string SupTyp { get; set; }       
        public string RegRev { get; set; }
        public string EcmGstin { get; set; }
        public string IgstOnIntra { get; set; }
        //public string Catg { get; set; }
        //public string Typ { get; set; }
        //public string EcmTrn { get; set; }
    }
}
