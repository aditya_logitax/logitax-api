﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace EwayBill.LogitaxAPImodule.EInvoicing
{
    public class sal_si_update_transporter_vm
    {
        public string ewbNo { get; set; }
        public string transporterId { get; set; }
    }
}