﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EwayBill.LogitaxAPImodule.EInvoicing
{
    public class get_EWaybill_DetailsBy_IRN_vm
    {
        public string CLIENTCODE { get; set; }
        public string USERCODE { get; set; }
        public string PASSWORD { get; set; }
        public string RequestorGSTIN { get; set; }
        public List<irn_vm> IRNList { get; set; }
    }
    public class irn_vm
    {
        public string IRN { get; set; }
    }


}
