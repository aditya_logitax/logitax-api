﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EwayBill.LogitaxAPImodule.EInvoicing
{
    public class e_invoice_reference_detail_vm
    {
        public string InvRm { get; set; }        
        public e_invoice_doc_perd_detail_vm DocPerdDtls { get; set; }
        public List<e_invoice_preceding_doc_detail_vm> PrecDocDtls { get; set; }
        public List<e_invoice_control_detail_vm> ContrDtls { get; set; }
    }
}
