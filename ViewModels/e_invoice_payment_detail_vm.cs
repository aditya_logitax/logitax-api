﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EwayBill.LogitaxAPImodule.EInvoicing
{
    public class e_invoice_payment_detail_vm
    {
        public string Nm { get; set; }
        public string AccDet { get; set; } 
        public string Mode { get; set; }
        public string FinInsBr { get; set; }
        public string PayTerm { get; set; }
        public string PayInstr { get; set; }
        public string CrTrn { get; set; }
        public string DirDr { get; set; }
        public int? CrDay { get; set; }
        public decimal? PaidAmt { get; set; }
        public decimal? PaymtDue { get; set; }   
        
    }
}
