﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EwayBill.LogitaxAPImodule.EInvoicing
{
    public class api_ewaybill_response_vm
    {
        public bool flag { get; set; }
        public string message { get; set; }
        public string EwbNo { get; set; }
        public string EwbDt { get; set; }
        public string EwbValidTill { get; set; }
        public string Status { get; set; }
        public string GenGstin { get; set; }
        public string Remarks { get; set; }
        public string ErrorDetails { get; set; }
        public string Irn { get; set; }
        public string pdfUrl { get; set; }
        public string detailedpdfUrl { get; set; }
        public dynamic InfoDtls { get; set; }
    }
}
