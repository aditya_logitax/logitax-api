﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EwayBill.LogitaxAPImodule.EInvoicing
{
    public class e_invoice_itemlist_detail_vm
    {
        public string SlNo { get; set; }
        public string PrdDesc { get; set; }
        public string IsServc { get; set; }
        public string HsnCd { get; set; }
        public string Barcde { get; set; }
        public decimal? Qty { get; set; }
        public decimal? FreeQty { get; set; }
        public string Unit { get; set; }
        public decimal UnitPrice { get; set; }
        public decimal TotAmt { get; set; }
        public decimal? Discount { get; set; }
        public decimal? PreTaxVal { get; set; }
        public decimal AssAmt { get; set; }
        public decimal GstRt { get; set; }
        public decimal? CgstAmt { get; set; }
        public decimal? SgstAmt { get; set; }
        public decimal? IgstAmt { get; set; }
        public decimal? CesAmt { get; set; }
        public decimal? CesRt { get; set; }
        public decimal? CesNonAdvlAmt { get; set; }
        public decimal? StateCesRt { get; set; }
        public decimal? StateCesAmt { get; set; }
        public decimal? StateCesNonAdvlAmt { get; set; }
        public decimal? OthChrg { get; set; }
        public decimal TotItemVal { get; set; }
        public string OrdLineRef { get; set; }
        public string OrgCntry { get; set; }
        public string PrdSlNo { get; set; }
        public List<e_invoice_attribute_detail_vm> AttribDtls { get; set; }
        public e_invoice_batch_detail_vm BchDtls { get; set; }
    }
}
