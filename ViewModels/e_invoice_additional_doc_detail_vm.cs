﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EwayBill.LogitaxAPImodule.EInvoicing
{
    public class e_invoice_additional_doc_detail_vm
    {
        public string Url { get; set; }
        public string Docs { get; set; }
        public string Info { get; set; }
    }
}
