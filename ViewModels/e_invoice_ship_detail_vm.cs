﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EwayBill.LogitaxAPImodule.EInvoicing
{
    public class e_invoice_ship_detail_vm
    {
        public string Gstin { get; set; }
        public string LglNm { get; set; }
        public string TrdNm { get; set; } 
        public string Addr1 { get; set; } 
        public string Addr2 { get; set; }
        public string Loc { get; set; }
        public int? Pin { get; set; }
        public string Stcd { get; set; }
    }
}
