﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EwayBill.LogitaxAPImodule.EInvoicing
{
    public class api_response
    {
        public bool flag { get; set; }
        public string message { get; set; }
        public string ewayBillNo { get; set; }
        public string ewayBillDate { get; set; }
        public string validUpto { get; set; }
        public string alert { get; set; }
        public string docNo { get; set; }
        public string docDate { get; set; }
        public string pdfUrl { get; set; }
        public string detailedpdfUrl { get; set; }
        public string JsonData { get; set; }
        public string error_log_id { get; set; }
    }

    public class api_cancel_response
    {
        public bool flag { get; set; }
        public string message { get; set; }
        public string Irn { get; set; }
        public string CancelDate { get; set; }
        public string error_log_id { get; set; }
    }

    public class api_ewb_cancel_response
    {
        public bool flag { get; set; }
        public string message { get; set; }
        public string ewayBillNo { get; set; }
        public string cancelDate { get; set; }
        public string error_log_id { get; set; }
    }

    public class api_vehicle_update_response
    {
        public bool flag { get; set; }
        public string message { get; set; }
        public string ewayBillNo { get; set; }
        public string validUpto { get; set; }
        public string vehUpdDate { get; set; }
        public string updatedDate { get; set; }
        public string error_log_id { get; set; }
    }
}
