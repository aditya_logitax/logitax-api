﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EwayBill.LogitaxAPImodule.EInvoicing
{
    public class api_e_invoice_vayana_response_vm
    {
        public api_e_invoice_vayana_response_vm()
        {
            ErrorDetails = new List<ErrorDetail>();
            Data = new api_e_invoice_vayana_data_response_vm();
        }

        public int Status { get; set; }
        public api_e_invoice_vayana_data_response_vm Data { get; set; }
        public List<ErrorDetail> ErrorDetails { get; set; }
        public dynamic InfoDtls { get; set; }
    }
    public class api_e_invoice_vayana_data_response_vm
    {
        public string AckNo { get; set; }
        public string AckDt { get; set; }
        public string Irn { get; set; }
        public string SignedInvoice { get; set; }
        public string SignedQRCode { get; set; }
        public string Status { get; set; }
        public string EwbNo { get; set; }
        public string EwbDt { get; set; }
        public string EwbValidTill { get; set; }
        public string Remarks { get; set; }
    }
}
