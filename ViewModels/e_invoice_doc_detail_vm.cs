﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EwayBill.LogitaxAPImodule.EInvoicing
{
    public class e_invoice_doc_detail_vm
    {
        public string Typ { get; set; }
        public string No { get; set; }
        public string Dt { get; set; }
        //public string OrgInvNo { get; set; }
    }
}
