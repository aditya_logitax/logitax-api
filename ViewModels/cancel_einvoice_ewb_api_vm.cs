﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EwayBill.LogitaxAPImodule.EInvoicing
{
    public class cancel_einvoice_ewb_api_vm
    {
        public string CLIENTCODE { get; set; }
        public string USERCODE { get; set; }
        public string PASSWORD { get; set; }
        public List<canceleinvewb_list_vm> cancelledeinvoiceewblist { get; set; }
    }

    public class canceleinvewb_list_vm
    {
        public string ewbNo { get; set; }
        public string cancelRsnCode { get; set; }
        public string cancelRmrk { get; set; }
    }
}
