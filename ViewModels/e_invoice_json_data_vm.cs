﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EwayBill.LogitaxAPImodule.EInvoicing
{
    public class e_invoice_json_data_basse64_vm
    {
        public string user_code { get; set; }
        public string client_code { get; set; }
        public string password { get; set; }
        public string template_code { get; set; }
        public string ERPType { get; set; }
        public string json_data { get; set; }
    }

    public class e_invoice_json_data_vm
    {
        public string user_code { get; set; }
        public string client_code { get; set; }
        public string password { get; set; }
        public string template_code { get; set; }
        public string ERPType { get; set; }
        public sal_e_invoice_detail_vm json_data { get; set; }
    }

    public class e_invoice_json_vm
    {
        public string user_code { get; set; }
        public string client_code { get; set; }
        public string password { get; set; }
        public string template_code { get; set; }
        public string ERPType { get; set; }
        public sal_e_invoice_detail_vm json_data { get; set; }
    }
}
