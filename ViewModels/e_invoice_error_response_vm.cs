﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EwayBill.LogitaxAPImodule.EInvoicing
{
    public class e_invoice_error_response_vm
    {
        public int Status { get; set; }
        public string Data { get; set; }
        public List<e_invoicing_detail_error_response_vm> ErrorDetails { get; set; }
        public string Info { get; set; }
        public List<e_invoice_response_info_details> InfoDtls { get; set; }
    }

    public class e_invoicing_detail_error_response_vm
    {
        public string ErrorCode { get; set; }
        public string ErrorMessage { get; set; }
    }
}
