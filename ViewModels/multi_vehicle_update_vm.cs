﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EwayBill.LogitaxAPImodule.EInvoicing
{

    public class multi_vehicle_update_vm
    {
        public string CLIENTCODE { get; set; }
        public string USERCODE { get; set; }
        public string PASSWORD { get; set; }
        public List<vehicle_update_list_vm> vehicleupdatelist { get; set; }
    }

    public class vehicle_update_list_vm
    {
        public Int64? ewbNo { get; set; }
        public string vehicleNo { get; set; }
        public string fromPlace { get; set; }
        public string fromState { get; set; }
        public string reasonCode { get; set; }
        public string reasonRem { get; set; }
        public string transDocNo { get; set; }
        public string transDocDate { get; set; }
        public string transMode { get; set; }
        public string vehicleType { get; set; }
    }
}
