﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EwayBill.LogitaxAPImodule.EInvoicing
{
    public class api_e_invoice_response_vm
    {
        public bool flag { get; set; }
        public string message { get; set; }
        public string DocNo { get; set; }
        public string DocDate { get; set; }
        public string AckNo { get; set; }
        public string AckDt { get; set; }
        public string Irn { get; set; }
        public string SignedInvoice { get; set; }
        public string SignedQRCode { get; set; }
        public string Status { get; set; }
        public string DcrySignedInvoice { get; set; }
        public string DcrySignedQRCode { get; set; }
        public string error_log_ids { get; set; }
        public string EwbNo { get; set; }
        public string EwbDt { get; set; }
        public string EwbValidTill { get; set; }
        public string pdfUrl { get; set; }
        public string detailedpdfUrl { get; set; }
        //public List<e_invoice_info_details> InfoDtls { get; set; }
        public dynamic InfoDtls { get; set; }
    }

    public class api_e_invoice_logitax_response_vm
    {
        public bool flag { get; set; }
        public string message { get; set; }
        public string DocNo { get; set; }
        public string DocDate { get; set; }
        public string DocType { get; set; }
        public string AckNo { get; set; }
        public string AckDt { get; set; }
        public string Irn { get; set; }
        public string SignedInvoice { get; set; }
        public string SignedQRCode { get; set; }
        public string Status { get; set; }
        public string DcrySignedInvoice { get; set; }
        public string DcrySignedQRCode { get; set; }
        public string error_log_ids { get; set; }
        public string EwbNo { get; set; }
        public string EwbDt { get; set; }
        public string EwbValidTill { get; set; }
        public string pdfUrl { get; set; }
        public string detailedpdfUrl { get; set; }
        //public List<e_invoice_info_details> InfoDtls { get; set; }
        public dynamic InfoDtls { get; set; }
    }
}
