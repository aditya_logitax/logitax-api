﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EwayBill.LogitaxAPImodule.EInvoicing
{
   public class e_invoice_export_detail_vm
    {
        public string ShipBNo { get; set; }
        public string ShipBDt { get; set; }
        public string Port { get; set; }
        public string RefClm { get; set; }        
        public string ForCur { get; set; }
        public string CntCode { get; set; }
        public decimal? ExpDuty { get; set; }
    }
    
}
