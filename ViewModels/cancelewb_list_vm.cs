﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EwayBill.LogitaxAPImodule.EInvoicing
{
    public class cancelewb_list_vm
    {
        public string EwbNo { get; set; }
        public string CancelledReason { get; set; }
        public string CancelledRemarks { get; set; }
    }

    public class cancelewb_vm
    {
        public string EwbNo { get; set; }
        public string cancelRsnCode { get; set; }
        public string cancelRmrk { get; set; }
    }

    public class rejectewb_vm
    {
       public string ewbNo { get; set; }
    }

    public class updatetransporterewb_vm
    {
        public string ewbNo { get; set; }
        public string transporterId { get; set; }
    }

    public class extendvalidityewb_vm
    {
        public long ewbNo { get; set; }
        public string vehicleNo { get; set; }
        public string fromPlace { get; set; }
        public string fromState { get; set; }
        public int remainingDistance { get; set; }
        public string transDocNo { get; set; }
        public string transDocDate { get; set; }
        public string transMode { get; set; }
        public int extnRsnCode { get; set; }
        public string extnRemarks { get; set; }
        public int fromPincode { get; set; }
        public string consignmentStatus { get; set; }
        public string transitType { get; set; }
        public string addressLine1 { get; set; }
        public string addressLine2 { get; set; }
        public string addressLine3 { get; set; }
    }
}
