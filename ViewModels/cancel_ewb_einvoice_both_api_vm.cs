﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EwayBill.LogitaxAPImodule.EInvoicing
{
    public class cancel_ewb_einvoice_both_api_vm
    {
        public string CLIENTCODE { get; set; }
        public string USERCODE { get; set; }
        public string PASSWORD { get; set; }
        public List<cancelewbeinvboth_list_vm> cancelewbeinvbothlist { get; set; }
    }

    public class cancelewbeinvboth_list_vm
    {
        public string Irn { get; set; }
        public string ewbNo { get; set; }
        public string cancelRsnCode { get; set; }
        public string cancelRmrk { get; set; }
    }

    public class api_ewb_einvoice_both_cancel_response
    {
        public bool flag { get; set; }
        public string message { get; set; }
        public string ewayBillNo { get; set; }
        public string Irn { get; set; }
        public string EWBCancelDate { get; set; }
        public string EInvoiceCancelDate { get; set; } 
        public string error_log_id { get; set; }
    }
}
