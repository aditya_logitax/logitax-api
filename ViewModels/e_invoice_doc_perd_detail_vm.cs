﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EwayBill.LogitaxAPImodule.EInvoicing
{
    public class e_invoice_doc_perd_detail_vm
    {
        public string InvStDt { get; set; }
        public string InvEndDt { get; set; }
    }
}
