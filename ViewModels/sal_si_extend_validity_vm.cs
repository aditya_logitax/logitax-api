﻿using System;
using System.Collections.Generic;

namespace EwayBill.LogitaxAPImodule.EInvoicing
{
    public class sal_si_extend_validity_vm
    {
        public Int64 ewbNo { get; set; }
        public string vehicleNo { get; set; }
        public string fromPlace { get; set; }
        public int fromState { get; set; }
        public int remainingDistance { get; set; }
        public string transDocNo { get; set; }
        public string transDocDate { get; set; }
        public string transMode { get; set; }
        public int? extnRsnCode { get; set; }
        public string extnRemarks { get; set; }
        public string consignmentStatus { get; set; }
        public string transitType { get; set; }
        public int? fromPincode { get; set; }
        public string addressLine1 { get; set; }
        public string addressLine2 { get; set; }
        public string addressLine3 { get; set; }
        public int? fromState_id { get; set; }
        public int? extnRsnCode_id { get; set; }
        public int? transMode_id { get; set; }
        public int? consignmentStatus_id { get; set; }
        public int? transitType_id { get; set; }

        public string updatedDate { get; set; }
        public string validUpto { get; set; }
        public string module_form_code { get; set; }
        public bool? schedule_later { get; set; }
        public DateTime? scheduled_det { get; set; }
    }

    public class multi_extend_ewb_vm
    {
        public string CLIENTCODE { get; set; }
        public string USERCODE { get; set; }
        public string PASSWORD { get; set; }
        public List<extend_validity_vm> extendewblist { get; set; }
    }

    public class extend_validity_vm
    {
        public Int64 ewbNo { get; set; }
        public string vehicleNo { get; set; }
        public string fromPlace { get; set; }
        public string fromState { get; set; }
        public int remainingDistance { get; set; }
        public string transDocNo { get; set; }
        public string transDocDate { get; set; }
        public string transMode { get; set; }
        public string extnRsnCode { get; set; }
        public string extnRemarks { get; set; }
        public string consignmentStatus { get; set; }
        public string transitType { get; set; }
        public int? fromPincode { get; set; }
        public string addressLine1 { get; set; }
        public string addressLine2 { get; set; }
        public string addressLine3 { get; set; }
    }

    public class extend_validity_response_vm
    {
        public string updatedDate { get; set; }
        public string validUpto { get; set; }
        public string ewayBillNo { get; set; }
    }
}
