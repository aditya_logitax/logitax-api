﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EwayBill.LogitaxAPImodule.EInvoicing
{
    public class api_e_invoice_duplicate_vm
    {
        public api_e_invoice_duplicate_vm()
        {
            ErrorDetails = new List<ErrorDetail>();
            InfoDtls = new List<InfoDtl>();
        }
        public int Status { get; set; }
        public List<ErrorDetail> ErrorDetails { get; set; }
        public object Data { get; set; }
        public List<InfoDtl> InfoDtls { get; set; }
    }

    public class ErrorDetail
    {
        public string ErrorCode { get; set; }
        public string ErrorMessage { get; set; }
    }

    public class Desc
    {
        public long AckNo { get; set; }
        public string AckDt { get; set; }
        public string Irn { get; set; }
    }

    public class InfoDtl
    {
        public string InfCd { get; set; }
        public Desc Desc { get; set; }
    }
}
