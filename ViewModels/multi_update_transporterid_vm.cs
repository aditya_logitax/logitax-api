﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EwayBill.LogitaxAPImodule.EInvoicing
{ 
    public class multi_update_transporterid_vm
    {
        public string CLIENTCODE { get; set; }
        public string USERCODE { get; set; }
        public string PASSWORD { get; set; }
        public List<sal_si_update_transporter_vm> transporteridlist { get; set; }
    }

    public class multi_update_transporterid_response_vm
    {
        public string ewayBillNo { get; set; }
        public bool flag { get; set; }
        public string message { get; set; }
        public string transporterId { get; set; }
        public string module_form_code { get; set; }
        public string transUpdateDate { get; set; }
        public string error_log_id { get; set; }
    }

}
