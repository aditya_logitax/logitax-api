﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EwayBill.LogitaxAPImodule.EInvoicing
{
    public class e_invoice_batch_detail_vm
    {
        public string SlNo { get; set; }
        public string Nm { get; set; }
        public string ExpDt { get; set; }
        public string WrDt { get; set; } 
    }
}
