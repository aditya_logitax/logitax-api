﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EwayBill.LogitaxAPImodule.EInvoicing
{
    public class sal_e_invoice_detail_vm
    {
        public string Version { get; set; }
        public e_invoice_trans_detail_vm TranDtls { get; set; }
        public e_invoice_doc_detail_vm DocDtls { get; set; }
        public e_invoice_seller_detail_vm SellerDtls { get; set; }
        public e_invoice_buyer_detail_vm BuyerDtls { get; set; } 
        public e_invoice_dispatch_detail_vm DispDtls { get; set; }
        public e_invoice_ship_detail_vm ShipDtls { get; set; }
        public e_invoice_value_detail_vm ValDtls { get; set; }
        public List<e_invoice_itemlist_detail_vm> ItemList { get; set; }
        public e_invoice_payment_detail_vm PayDtls { get; set; }
        public e_invoice_reference_detail_vm RefDtls { get; set; }
        public e_invoice_export_detail_vm ExpDtls { get; set; }      
        public List<e_invoice_additional_doc_detail_vm> AddlDocDtls { get; set; }
        public e_invoice_ewb_detail_vm EwbDtls { get; set; }

    }
}
