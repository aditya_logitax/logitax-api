﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EwayBill.LogitaxAPImodule.EInvoicing
{
    public class get_ewb_e_invoice_vm
    {
        public string CLIENTCODE { get; set; }
        public string USERCODE { get; set; }
        public string PASSWORD { get; set; }
        public List<ewb_e_invoice_list_vm> ewbeinvoicelist { get; set; }
    }

    public class ewb_e_invoice_list_vm
    {
        public string Irn { get; set; }
        public int? Distance { get; set; }
        public string TransMode { get; set; }
        public string TransId { get; set; }
        public string TransName { get; set; }
        public string TransDocDt { get; set; }
        public string TransDocNo { get; set; }
        public string VehNo { get; set; }
        public string VehType { get; set; }
        public exp_ship_dtls ExpShipDtls { get; set; }
        public disp_dtls DispDtls { get; set; }
    }
    public class exp_ship_dtls
    {
        public string Addr1 { get; set; }
        public string Addr2 { get; set; }
        public string Loc { get; set; }
        public int? Pin { get; set; }
        public string Stcd { get; set; }

    }
    public class disp_dtls
    {
        public string Nm { get; set; }
        public string Addr1 { get; set; }
        public string Addr2 { get; set; }
        public string Loc { get; set; }
        public int? Pin { get; set; }
        public string Stcd { get; set; }
    }
}
