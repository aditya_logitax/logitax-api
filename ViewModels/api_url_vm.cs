﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EwayBill.LogitaxAPImodule.EInvoicing
{
    public class api_url_vm
    {
        public string APIUrlName
        {
            get;
            set;
        }
        public string APIUrlType
        {
            get;
            set;
        }
        public string APIUrl
        {
            get;
            set;
        }
    }

    public static class api_url_list_vm
    {
        public static List<api_url_vm> api_url_list = new List<api_url_vm>();
    }
}
