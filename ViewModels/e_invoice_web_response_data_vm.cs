﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EwayBill.LogitaxAPImodule.EInvoicing
{
    public class e_invoice_status_vm
    {
        public int Status { get; set; }
        public string Data { get; set; }
        public object Decrypt_Data { get; set; }
        public string error { get; set; }
        public List<e_invoicing_detail_response_vm> ErrorDetails { get; set; }
        public string api_log_id { get; set; }
        public string error_code { get; set; }
        public string error_message { get; set; }
        public int? si_id { get; set; }
    }

    public class e_invoice_web_response_data_vm
    {
        public int Status { get; set; }
        public string Data { get; set; }
        public e_invoice_data_response Dcy_Data { get; set; }
        public List<e_invoicing_detail_response_vm> ErrorDetails { get; set; }
        public string Info { get; set; }
        public string DocNo { get; set; }
        public string DocDate { get; set; }
        public List<e_invoice_info_details> InfoDtls { get; set; }
        public string api_log_id { get; set; }
        public string error_code { get; set; }
        public string error_message { get; set; }
        public int? si_id { get; set; }
    }

    public class ewb_response_data_vm
    {
        public int Status { get; set; }
        public string Data { get; set; }
        public ewb_data_response_1 Dcy_Data { get; set; }
        public List<e_invoicing_detail_response_vm> ErrorDetails { get; set; }
        public string Info { get; set; }
        public string DocNo { get; set; }
        public string DocDate { get; set; }
        public List<e_invoice_info_details> InfoDtls { get; set; }
        public string api_log_id { get; set; }
        public string error_code { get; set; }
        public string error_message { get; set; }
        public int? si_id { get; set; }
    }

    public class e_invoice_web_response_data_vm_1
    {
        public int Status { get; set; }
        public string Data { get; set; }
        public e_invoice_data_response Dcy_Data { get; set; }
        public List<e_invoicing_detail_response_vm> ErrorDetails { get; set; }
        public string Info { get; set; }
        public string DocNo { get; set; }
        public string DocDate { get; set; }
        public List<e_invoice_info_details_dy> InfoDtls { get; set; }
        public string api_log_id { get; set; }
        public string error_code { get; set; }
        public string error_message { get; set; }
        public int? si_id { get; set; }
    }
    public class e_invoice_web_api_success_response_vm
    {
        public int Status { get; set; }
        public string Data { get; set; }
        public e_invoice_data_response Dcy_Data { get; set; }
        public List<e_invoicing_detail_response_vm> ErrorDetails { get; set; }
        public string Info { get; set; }
        public string DocNo { get; set; }
        public string DocDate { get; set; }
        public List<e_invoice_success_response_info_details> InfoDtls { get; set; }
    }
    public class e_invoice_web_response_vm
    {
        public int Status { get; set; }
        public string Data { get; set; }
        public e_invoice_data_response Dcy_Data { get; set; }
        public List<e_invoicing_detail_response_vm> ErrorDetails { get; set; }
        public string Info { get; set; }
        public string DocNo { get; set; }
        public string DocDate { get; set; }
        public List<e_invoice_response_info_details> InfoDtls { get; set; }
        public string api_log_id { get; set; }
        public string error_code { get; set; }
        public string error_message { get; set; }
        public int? si_id { get; set; }
    }
    public class e_invoice_info_details
    {
        public string InfCd { get; set; }
        public List<e_invoicing_detail_response_vm> Desc { get; set; }
        public e_invoice_cancel_response_vm cancDesc { get; set; }
    }

    public class e_invoice_info_details_dy
    {
        public string InfCd { get; set; }
        public dynamic Desc { get; set; }
        public e_invoice_cancel_response_vm cancDesc { get; set; }
    }

    public class e_invoice_response_info_details
    {
        public string InfCd { get; set; }
        public e_invoice_data_response Desc { get; set; }
        public e_invoice_cancel_response_vm cancDesc { get; set; }
    }

    public class e_invoice_success_response_info_details
    {
        public string InfCd { get; set; }
        public string Desc { get; set; }
    }
    public class e_invoice_data_response
    {
        public string AckNo { get; set; }
        public string AckDt { get; set; }
        public string Irn { get; set; }
        public string SignedInvoice { get; set; }
        public string SignedQRCode { get; set; }
        public string Status { get; set; }
        public string DocNo { get; set; }
        public string DocDate { get; set; }
        public string DcrySignedInvoice { get; set; }
        public string DcrySignedQRCode { get; set; }
        public string EwbNo { get; set; }
        public string EwbDt { get; set; }
        public string EwbValidTill { get; set; }
        public string api_log_id { get; set; }
        public int ewb_status_id { get; set; }
    }
    public class ewb_data_response
    {
        public bool flag { get; set; }
        public string message { get; set; }
        public string Irn { get; set; }
        public string EwbNo { get; set; }
        public string EwbDt { get; set; }
        public string EwbValidTill { get; set; }
        public string pdfUrl { get; set; }
        public string detailedpdfUrl { get; set; }
        public List<e_invoice_info_details_dy> InfoDtls { get; set; }
        public string Remarks { get; set; }
        public string error_log_id { get; set; }
    }

    public class ewb_data_response_1
    {
        public bool flag { get; set; }
        public string message { get; set; }
        public string EwbNo { get; set; }
        public string EwbDt { get; set; }
        public string EwbValidTill { get; set; }
        public string Status { get; set; }
        public string GenGstin { get; set; }
        public string Remarks { get; set; }
        public string ErrorDetails { get; set; }
        public string InfoDtls { get; set; }
        public string Data { get; set; }
}

}
