﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EwayBill.LogitaxAPImodule.EInvoicing
{
    public class e_invoice_attribute_detail_vm
    {
        public string Nm { get; set; }
        public string Val { get; set; }
        public string SlNo { get; set; }
    }
}
