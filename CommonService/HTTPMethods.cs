﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace EwayBill.LogitaxAPImodule.CommonService
{
    public class HTTPMethods
    {
        public string HTTPPostMethod(string request, string URL)
        {
            SecurityProtocolType origSecurityProtocol = System.Net.ServicePointManager.SecurityProtocol;
            ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;
            HttpWebRequest httprequest = (HttpWebRequest)WebRequest.Create(URL);
            httprequest.Method = "POST";
            httprequest.KeepAlive = true;
            httprequest.AllowAutoRedirect = false;
            httprequest.Accept = "*/*";
            httprequest.ContentType = "application/json";
            httprequest.Timeout = 300000;
            httprequest.ContentLength = request.Length;
            using (StreamWriter streamWriter = new StreamWriter(httprequest.GetRequestStream()))
            {
                streamWriter.Write(request);
                streamWriter.Flush();
                streamWriter.Close();
            }
            WebResponse httpresponse = httprequest.GetResponse();
            string response = new StreamReader(httpresponse.GetResponseStream()).ReadToEnd();
            return response;
        }

        public string HTTPGetMethod(string request, string URL)
        {
            SecurityProtocolType origSecurityProtocol = System.Net.ServicePointManager.SecurityProtocol;
            ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;
            HttpWebRequest httprequest = (HttpWebRequest)WebRequest.Create(URL);
            httprequest.Method = "Get";
            httprequest.KeepAlive = true;
            httprequest.AllowAutoRedirect = false;
            httprequest.Accept = "*/*";
            httprequest.ContentType = "application/json";
            httprequest.Timeout = 300000;
            httprequest.ContentLength = request.Length;
            WebResponse httpresponse = httprequest.GetResponse();
            string response = new StreamReader(httpresponse.GetResponseStream()).ReadToEnd();
            return response;
        }
    }
}
