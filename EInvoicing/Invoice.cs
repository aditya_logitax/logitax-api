﻿using EwayBill.LogitaxAPImodule.CommonService;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EwayBill.LogitaxAPImodule.EInvoicing
{
    public class Invoice
    {
        HTTPMethods hTTPMethods;
        string URL = string.Empty;
        public Invoice()
        {
            hTTPMethods = new HTTPMethods();
        }
        public List<api_e_invoice_response_vm> GenerateJSONEInvoice(e_invoice_json_data_vm content,string APIUrlType)
        {
            var requestjson = JsonConvert.SerializeObject(content);
            if (APIUrlType == "DEMO")
            {
                URL = @"https://uat.logitax.in/DEMO/EInvoiceWebApi/GenerateJSONEInvoice";
            }

            else if (APIUrlType == "PRD")
            {
                URL = "https://app.logitax.in/EInvoiceWebApi/GenerateJSONEInvoice";
            }
            var resoponse = hTTPMethods.HTTPPostMethod(requestjson, URL);
            var resoponsejson = JsonConvert.DeserializeObject<List<api_e_invoice_response_vm>>(resoponse);
            return resoponsejson;
        }

        public List<ewb_data_response> GenerateEWBEinvoice(get_ewb_e_invoice_vm content, string APIUrlType)
        {
            var requestjson = JsonConvert.SerializeObject(content);
            if (APIUrlType == "DEMO")
            {
                URL = @"https://uat.logitax.in/DEMO/TransactionAPI/GenerateEWBEinvoice";
            }

            else if (APIUrlType == "PRD")
            {
                URL = "https://app.logitax.in/TransactionAPI/GenerateEWBEinvoice";
            }
            var resoponse = hTTPMethods.HTTPPostMethod(requestjson, URL);
            var resoponsejson = JsonConvert.DeserializeObject<List<ewb_data_response>>(resoponse);
            return resoponsejson;
        }

        public List<api_cancel_response> CancelEInvoice(cancel_einvoice_api_vm content, string APIUrlType)
        {
            var requestjson = JsonConvert.SerializeObject(content);
            if (APIUrlType == "DEMO")
            {
                URL = @"https://uat.logitax.in/DEMO/TransactionAPI/CancelEInvoice";
            }

            else if (APIUrlType == "PRD")
            {
                URL = "https://app.logitax.in/TransactionAPI/CancelEInvoice";
            }
            var resoponse = hTTPMethods.HTTPPostMethod(requestjson, URL);
            var resoponsejson = JsonConvert.DeserializeObject<List<api_cancel_response>>(resoponse);
            return resoponsejson;
        }

        public List<api_ewb_cancel_response> CancelEWB(cancel_ewb_api_vm content, string APIUrlType)
        {
            var requestjson = JsonConvert.SerializeObject(content);
            if (APIUrlType == "DEMO")
            {
                URL = @"https://uat.logitax.in/DEMO/TransactionAPI/CancelEInvoice";
            }

            else if (APIUrlType == "PRD")
            {
                URL = "https://app.logitax.in/TransactionAPI/CancelEInvoice";
            }
            var resoponse = hTTPMethods.HTTPPostMethod(requestjson, URL);
            var resoponsejson = JsonConvert.DeserializeObject<List<api_ewb_cancel_response>>(resoponse);
            return resoponsejson;
        }

        public List<api_ewb_cancel_response> CancelEInvoiceEWB(cancel_einvoice_ewb_api_vm content, string APIUrlType)
        {
            var requestjson = JsonConvert.SerializeObject(content);
            if (APIUrlType == "DEMO")
            {
                URL = @"https://uat.logitax.in/DEMO/TransactionAPI/CancelEInvoiceEWB";
            }

            else if (APIUrlType == "PRD")
            {
                URL = "https://app.logitax.in/TransactionAPI/CancelEInvoiceEWB";
            }
            var resoponse = hTTPMethods.HTTPPostMethod(requestjson, URL);
            var resoponsejson = JsonConvert.DeserializeObject<List<api_ewb_cancel_response>>(resoponse);
            return resoponsejson;
        }

        public List<api_ewb_einvoice_both_cancel_response> CancelEWBEinvoiceBoth(cancel_ewb_einvoice_both_api_vm content, string APIUrlType)
        {
            var requestjson = JsonConvert.SerializeObject(content);
            if (APIUrlType == "DEMO")
            {
                URL = @"https://uat.logitax.in/DEMO/TransactionAPI/CancelEWBEinvoiceBoth";
            }

            else if (APIUrlType == "PRD")
            {
                URL = "https://app.logitax.in/TransactionAPI/CancelEWBEinvoiceBoth";
            }
            var resoponse = hTTPMethods.HTTPPostMethod(requestjson, URL);
            var resoponsejson = JsonConvert.DeserializeObject<List<api_ewb_einvoice_both_cancel_response>>(resoponse);
            return resoponsejson;
        }

        public List<api_ewaybill_response_vm> GetEWaybillDetailsByIRN(get_EWaybill_DetailsBy_IRN_vm content, string APIUrlType)
        {
            var requestjson = JsonConvert.SerializeObject(content);
            if (APIUrlType == "DEMO")
            {
                URL = @"https://uat.logitax.in/DEMO/TransactionAPI/GetEWaybillDetailsByIRN";
            }

            else if (APIUrlType == "PRD")
            {
                URL = "https://app.logitax.in/TransactionAPI/GetEWaybillDetailsByIRN";
            }
            var resoponse = hTTPMethods.HTTPPostMethod(requestjson, URL);
            var resoponsejson = JsonConvert.DeserializeObject<List<api_ewaybill_response_vm>>(resoponse);
            return resoponsejson;
        }

        public List<api_e_invoice_response_vm> GetIRNByDocDetails(get_IRN_details_by_docdetails_vm content, string APIUrlType)
        {
            var requestjson = JsonConvert.SerializeObject(content);
            if (APIUrlType == "DEMO")
            {
                URL = @"https://uat.logitax.in/DEMO/TransactionAPI/GetIRNByDocDetails";
            }

            else if (APIUrlType == "PRD")
            {
                URL = "https://app.logitax.in/TransactionAPI/GetIRNByDocDetails";
            }
            var resoponse = hTTPMethods.HTTPPostMethod(requestjson, URL);
            var resoponsejson = JsonConvert.DeserializeObject<List<api_e_invoice_response_vm>>(resoponse);
            return resoponsejson;
        }

        public List<api_vehicle_update_response> UpdateVehicle(multi_vehicle_update_vm content, string APIUrlType)
        {

            var requestjson = JsonConvert.SerializeObject(content);
            if (APIUrlType == "DEMO")
            {
                URL = @"https://uat.logitax.in/DEMO/TransactionAPI/UpdateVehicle";
            }

            else if (APIUrlType == "PRD")
            {
                URL = "https://app.logitax.in/TransactionAPI/UpdateVehicle";
            }
            var resoponse = hTTPMethods.HTTPPostMethod(requestjson, URL);
            var resoponsejson = JsonConvert.DeserializeObject<List<api_vehicle_update_response>>(resoponse);
            return resoponsejson;
        }

        public List<multi_update_transporterid_response_vm> UpdateTransporterId(multi_update_transporterid_vm content, string APIUrlType)
        {
            var requestjson = JsonConvert.SerializeObject(content);
            if (APIUrlType == "DEMO")
            {
                URL = @"https://uat.logitax.in/DEMO/TransactionAPI/UpdateTransporterId";
            }

            else if (APIUrlType == "PRD")
            {
                URL = "https://app.logitax.in/TransactionAPI/UpdateTransporterId";
            }
            var resoponse = hTTPMethods.HTTPPostMethod(requestjson, URL);
            var resoponsejson = JsonConvert.DeserializeObject<List<multi_update_transporterid_response_vm>>(resoponse);
            return resoponsejson;
        }

        public List<api_vehicle_update_response> ExtendEWB(multi_extend_ewb_vm content, string APIUrlType)
        {
            var requestjson = JsonConvert.SerializeObject(content);
            if (APIUrlType == "DEMO")
            {
                URL = @"https://uat.logitax.in/DEMO/TransactionAPI/ExtendEWB";
            }

            else if (APIUrlType == "PRD")
            {
                URL = "https://app.logitax.in/TransactionAPI/ExtendEWB";
            }
            var resoponse = hTTPMethods.HTTPPostMethod(requestjson, URL);
            var resoponsejson = JsonConvert.DeserializeObject<List<api_vehicle_update_response>>(resoponse);
            return resoponsejson;
        }
    }
}
